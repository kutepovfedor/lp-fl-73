<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form".
 *
 * @property int $id
 * @property string $phone
 * @property string $email
 * @property string $name
 * @property string $utm
 * @property string $create_at
 * @property string $update_at
 */
class Form extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'name'], 'required'],
            [['inst', 'whats', 'fb', 'telega'], 'integer'],
            [['utm'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['phone', 'email', 'name'], 'string', 'max' => 512],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Ваш телефон',
            'email' => 'Ваш E-mail',
            'name' => 'Ваше Имя',
            'utm' => 'Utm',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
