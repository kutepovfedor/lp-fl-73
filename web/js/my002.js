$(function(){
	$('.slick-screen-2').slick({
		infinite: true,
		dots: true,
		arrows: false,
	});
	$('.slick-screen-7').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
	});
	$('.slick-screen-mobile-7').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
	});
	$('.slick-screen-mobile-10').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots:true
	});
	$('.slick-screen-8').slick({
		infinite: true,
		dots: true,
		arrows: false,
		slidesToShow: 2,
		slidesToScroll: 2
	});
	$('#retroclockbox1').flipcountdown({
		// size:"lg"
	});

    $('.img-in').on('touchstart mouseover',function(){
        var number = $(this).attr('data-number');
        var selector = $(this);
        $(selector).attr('src', '/img/screen-11-'+number+'-in.png');
        $(selector).hide();
        $(selector).fadeIn(500);
  //       $(selector).show(500, function(){
  //       	$(selector).attr('src', '/img/screen-11-'+number+'-in.png');
		// });
    });

    $('.img-in').on('touchend touchmove mouseout',function(){
        var number = $(this).attr('data-number');
        $(this).attr('src', '/img/screen-11-'+number+'.png');
    });
});