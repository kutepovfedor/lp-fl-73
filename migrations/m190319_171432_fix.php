<?php

use yii\db\Migration;

/**
 * Class m190319_171432_fix
 */
class m190319_171432_fix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `form`
    CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190319_171432_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190319_171432_fix cannot be reverted.\n";

        return false;
    }
    */
}
