<?php

use yii\db\Migration;

/**
 * Class m190401_062853_new
 */
class m190401_062853_new extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `form`
    ADD COLUMN `inst` int(2) NULL AFTER `name`;
    ALTER TABLE `form`
    ADD COLUMN `whats` int(2) NULL AFTER `inst`;
    ALTER TABLE `form`
    ADD COLUMN `fb` int(2) NULL AFTER `whats`;
    ALTER TABLE `form`
    ADD COLUMN `telega` int(2) NULL AFTER `fb`;
    ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190401_062853_new cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190401_062853_new cannot be reverted.\n";

        return false;
    }
    */
}
