<?php

use yii\db\Migration;

/**
 * Class m190319_160454_utm_add
 */
class m190319_160454_utm_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `form`
    ADD COLUMN `utm` TEXT NULL AFTER `name`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190319_160454_utm_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190319_160454_utm_add cannot be reverted.\n";

        return false;
    }
    */
}
