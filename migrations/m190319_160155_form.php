<?php

use yii\db\Migration;

/**
 * Class m190319_160155_form
 */
class m190319_160155_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `form` (
            `id` INT(11) NOT NULL,
            `phone` VARCHAR(512) NOT NULL,
            `email` VARCHAR(512) NULL,
            `name` VARCHAR(512) NOT NULL,
            `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `update_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190319_160155_form cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190319_160155_form cannot be reverted.\n";

        return false;
    }
    */
}
