<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?= $content ?>
</div>
<?php if(!\Yii::$app->mobileDetect->isMobile()) :  ?>
<footer class="footer">
    <div class="container" style="padding-top:40px;padding-bottom:40px;">
        <div class="row">
            <div class="col-md-3">
                <img src="/img/logo-2.png" width="50%">
            </div> 
            <div class="col-md-3">
                <p style="color:#666;margin-top: 20px;font-size:15px;"><b>PRODUCT</b></p>
                <a style="color:#fff;font-size:13px;">Home page</a><br />
                <a style="color:#fff;font-size:13px;">Tour</a><br />
                <a style="color:#fff;font-size:13px;">Templates</a><br />
                <a style="color:#fff;font-size:13px;">Prices</a><br />
            </div> 
            <div class="col-md-3">
                <p style="color:#666;margin-top: 20px;font-size:15px;"><b>EDUCATION</b></p>
                <a style="color:#fff;font-size:13px;">Workshop</a><br />
                <a style="color:#fff;font-size:13px;">How to make website</a><br />
                <a style="color:#fff;font-size:13px;">Design course</a><br />
                <a style="color:#fff;font-size:13px;">Explore</a><br />
                
            </div>
            <div class="col-md-3">
                <p style="color:#666;margin-top: 20px;font-size:15px;"><b>HELP</b></p>
                <a style="color:#fff;font-size:13px;">Knowledge base</a><br />
                <a style="color:#fff;font-size:13px;">Viedo tutorials</a><br />
                <a style="color:#fff;font-size:13px;">Code export</a><br />
                <a style="color:#fff;font-size:13px;">Developers</a><br />
                
            </div> 
        </div>
    </div>
</footer>
<?php else : ?>
<footer class="footer">
    <div class="container" style="padding-top:40px;padding-bottom:40px;">
        <div class="row">
            <div class="col-xs-6">
                <img src="/img/logo-2.png" width="50%">
            </div> 
            <div class="col-xs-6">
                <p style="color:#666;margin-top: 20px;font-size:15px;"><b>PRODUCT</b></p>
                <a style="color:#fff;font-size:13px;">Home page</a><br />
                <a style="color:#fff;font-size:13px;">Tour</a><br />
                <a style="color:#fff;font-size:13px;">Templates</a><br />
                <a style="color:#fff;font-size:13px;">Prices</a><br />
            </div> 
        </div>
    </div>
</footer>
<?php endif?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
